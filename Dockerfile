FROM python:3.7-stretch
LABEL maintainer="peter.bourgonje@dfki.de"


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y
    
RUN python -m pip install --upgrade pip
ADD requirements.txt .
RUN pip install -r requirements.txt
RUN git clone https://github.com/huggingface/neuralcoref.git
RUN cd neuralcoref && pip install -r requirements.txt && pip install -e .
RUN pip install spacy==2.3.4 
RUN python -m spacy download en


ADD resolution.py .
ADD nif.py .
ADD flaskController.py .

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 5000

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=5000



