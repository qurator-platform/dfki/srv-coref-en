# -*- coding: utf-8 -*-

from nif import Nif
import sys
#import stanza
#from stanza.server import CoreNLPClient
import neuralcoref # probably has to be built from source (comment PB: indeed), I got the following error when installing the 4.0 version through pip: /usr/lib/python3.6/importlib/_bootstrap.py:219: RuntimeWarning: spacy.morphology.Morphology size changed, may indicate binary incompatibility. Expected 104 from C header, got 112 from PyObject
#pip uninstall neuralcoref
#git clone https://github.com/huggingface/neuralcoref.git
#cd neuralcoref
#pip install -r requirements.txt
#pip install -e .
#pip uninstall spacy
#pip install spacy
#python -m spacy download en # run as administrator
# from https://github.com/huggingface/neuralcoref/issues/222
import spacy

SUPPORTED_NIFFORMATS = ['turtle', 'xml', 'json-ld']

nlp = spacy.load('en')    
neuralcoref.add_to_pipe(nlp)
    

"""
def resolve_STNF(inp, informat, outformat, lang): # function extremely slow!
#required: stanza and CoreNLP https://stanfordnlp.github.io/stanza/client_setup.html 
#stanza.install_corenlp(dir="YOUR_CORENLP_FOLDER")
#stanza.download_corenlp_models(model='german', version='4.2.0', dir="YOUR_CORENLP_FOLDER")
#stanza.download_corenlp_models(model='english', version='4.1.0', dir="YOUR_CORENLP_FOLDER")    
    with CoreNLPClient(properties=lang, annotators='coref') as client: 
        ann = client.annotate(inp)
        
        nlp = stanza.Pipeline(lang='en', processors='tokenize')
        doc = nlp(inp)
        sents = []
        for sentence in doc.sentences:
            toks = list([token.text for token in sentence.tokens])
            sents.append(toks)
        for chain in ann.corefChain:
            mentions=[]  
            proto = chain.representative
            for m in chain.mention:
                mentions.append([m.sentenceIndex, m.beginIndex, m.endIndex-1])
            proto_indexes = mentions[proto]
            proto_str = sents[proto_indexes[0]][proto_indexes[1]]
            while proto_indexes[1] < proto_indexes[2]:
                proto_indexes[1] += 1
                proto_str += " " + sents[proto_indexes[0]][proto_indexes[1]]
            #print(proto_str)
            for m in mentions:
                if m == proto_indexes:
                    continue
                else:
                    sents[m[0]][m[1]] = proto_str
                    while m[1] < m[2]:
                        m[1] += 1
                        del sents[m[0]][m[1]]
    #print(sents)
        
    result = ""
    i = 0
    for sentence in sents:
        for token in sentence:
            if not token in [".", ",", ":", ";", "?", "!"]:
                if not i == 0:
                    result += " " + token
                else: 
                    result += token
            else:
                result += token
            i += 1
        result += " "
    return result
"""

def replace_insitu(inp, informat): # only available for plain text, hence no output format to be specified

    doc = nlp(inp)
    clusters = doc._.coref_clusters
    nifmodel = Nif()

    if informat == 'txt':
        nifmodel.initNifModelFromString(inp)
    elif informat in SUPPORTED_NIFFORMATS:
        nifmodel.parseModelFromString(inp, informat)
    else:
        nifmodel.initNifModelFromString(inp)
        sys.stderr.write('ERROR: Input format "%s" not supported. Interpreting input as plaintext.\n' % informat)
    plaintextinput = nifmodel.extractIsString()

    doc = nlp(plaintextinput)
    
    return doc._.coref_resolved

def resolve_nif(inp, informat, outformat):

    doc = nlp(inp)
    clusters = doc._.coref_clusters
    nifmodel = Nif()

    if informat == 'txt':
        nifmodel.initNifModelFromString(inp)
    elif informat in SUPPORTED_NIFFORMATS:
        nifmodel.parseModelFromString(inp, informat)
    else:
        nifmodel.initNifModelFromString(inp)
        sys.stderr.write('ERROR: Input format "%s" not supported. Interpreting input as plaintext.\n' % informat)
    plaintextinput = nifmodel.extractIsString()

    doc = nlp(plaintextinput)

    clusters = doc._.coref_clusters
    nifmodel.addMentions(clusters)
            
    nifString = None
    if outformat in SUPPORTED_NIFFORMATS:
        nifString = nifmodel.model.serialize(format=outformat).decode('utf-8')
    else:
        sys.stderr.write('ERROR: "turtle" is the only supported output format for now. Returning None, will probably crash afterwards...\n')
    
    return nifString

if __name__ == '__main__':
    
    #inp = "Chris Manning is a young man who works for Google. He likes the organization he works for."
    inp = "Chris Manning is a young man. He works at Google. Google is an organization. Angela Merkel lives in Berlin. She speaks German."
   # inp = "Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29. Mr. Vinken is chairman of Elsevier N.V."
    #inp = "Every time I visit him, Chris Manning bakes cookies." # STNF model does not seem to recognize cataphoras
    #print(resolve_STNF(inp, informat='txt', outformat='txt', lang='english'))
    #print(resolve_spacy(inp, informat="txt", outformat="json-ld"))

    #print(replace_insitu(inp, informat='txt'))

    #nifjson = resolve_nif(inp, informat='txt', outformat='json-ld')
    #print(json.dumps(json.loads(nifjson), indent=2))

    nifturtle = resolve_nif(inp, informat='txt', outformat='turtle')
    print(nifturtle)

    nifinput =     """
    @prefix itsrsdf: <http://www.w3.org/2005/11/its/rdf#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://qurator.ai/documents/1607513441.4140918#char=0,126> a nif:Context,
        nif:String ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "126"^^xsd:nonNegativeInteger ;
    nif:isString "Chris Manning is a young man. He works at Google. Google is an organization. Angela Merkel lives in Berlin. She speaks German." .

<http://qurator.ai/documents/1607513441.4140918#char=50,56> a nif:String ;
    nif:anchorOf "Google" ;
    nif:beginIndex "50"^^xsd:nonNegativeInteger ;
    nif:corefChainRoot <http://qurator.ai/documents/1607513441.4140918#char=42,48> ;
    nif:endIndex "56"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef <http://dbpedia.org/ontology/Organisation> .

<http://qurator.ai/documents/1607513441.4140918#char=0,13> a nif:String ;
    nif:anchorOf "Chris Manning" ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:corefChainRoot <http://qurator.ai/documents/1607513441.4140918#char=0,13> ;
    nif:endIndex "13"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef <http://dbpedia.org/ontology/Person> .

    """

    #nifturtle = resolve_nif(nifinput, informat='turtle', outformat='turtle')
    #print(nifturtle)
