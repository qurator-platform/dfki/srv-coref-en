rdflib==4.2.2
flask==1.0.2
flask_cors==3.0.7
rdflib-jsonld==0.4.0
spacy==2.3.4
numpy==1.16.1
