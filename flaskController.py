#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename

import resolution
"""


then to start run:
export FLASK_APP=flaskController.py (ON WINDOWS: 'set' instead of 'export')
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/resolve?input="Pierre Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29. Mr. Vinken is chairman of Elsevier N.V."&informat=txt&outformat=txt

curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr



"""


app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

@app.route('/', methods=['GET'])
@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"




##################### Coreference Resolution #####################
# two endpoints: one for NIF, on to just in-situ replace all found mentions with their full (chain root) surface form (i.e. 'Chris Manning works at google. He is a happy fellow.' -> 'Chris Manning works at google. Chris Manning is a happy fellow.'

@app.route('/replace', methods=['GET'])
def replace():

    supported_informats = ['txt']
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    if request.args.get('informat') == None: # superfluous, as it only accepts txt anyway...
        return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    elif request.args.get('informat') not in supported_informats:
        return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
    informat = request.args.get('informat')
    
    if request.args.get('input') == None:
        return 'Please provide some text as input.\n'

    replaced = resolution.replace_insitu(inp, informat)
        
    return replaced

    


@app.route('/resolve', methods=['GET'])
def resolve():

    """
    Required args:
    - informat (txt)
    - outformat (turtle, other nif-usuals, txt)
    - input (actual input text to perform coref resolution on)
    **only English supported**
    """
    
    supported_informats = ['txt', 'turtle', 'json-ld', 'xml']
    #supported_informats.extend(resolution.SUPPORTED_NIFFORMATS)
    supported_outformats = resolution.SUPPORTED_NIFFORMATS
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    if request.args.get('informat') == None:
        return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    elif request.args.get('informat') not in supported_informats:
        return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
    if request.args.get('outformat') == None:
        return 'Please specify output format (currently supported: %s)\n' % str(supported_outformats)
    elif request.args.get('outformat') not in supported_outformats:
        return 'Output format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('outformat'), (str(supported_outformats)))

    informat = request.args.get('informat')
    outformat = request.args.get('outformat')
    
    if request.args.get('input') == None:
        return 'Please provide some text as input.\n'

    nifString = resolution.resolve_nif(inp, informat, outformat)
        
    return nifString




    
if __name__ == '__main__':

    port = int(os.environ.get('PORT',5000))
    app.run(host='localhost', port=port, debug=True)
